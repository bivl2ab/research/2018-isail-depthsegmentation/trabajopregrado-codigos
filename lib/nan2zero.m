function I = nan2zero(I)
  I(isnan(I)) = 0; 
end