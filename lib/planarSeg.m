function segPlanes = planarSeg(x3, y3, z3)

mbs = fitAHCPlane(cat(3,x3,y3,z3));
segPlanes = zeros(425,560);
for i=1:length(mbs)
  mb=mbs{i};
  segPlanes(mb)=i;
end

end