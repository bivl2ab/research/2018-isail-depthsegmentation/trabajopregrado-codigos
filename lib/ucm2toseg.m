function seg = ucm2toseg(ucm2, k)
    labels2 = bwlabel(ucm2 <= k);
    seg = labels2(2:2:end, 2:2:end);
end    