function U = zunify(L, L0)
% Unify segmentation results
% SINTAX:
%     U = zunify(L_SEG, L_SAF);
% U         Unified Labels
% L_SEG     Segmentation (classical).
% L_SAF     Focus-based segmentation.
% 
% Said Pertuz
% Feb01/2013

RList = unique(L(:));   %List of segmented regions.
CList = unique(L0(:));  %List of clusters from SFA.�
R = numel(RList);       %No. of segmented regions.
C = numel(CList);       %No. of clusters of SFA.
U = zeros(size(L));
for r = 1:R
    mask1 = (L==RList(r)); %Current region.

    %Compare with clusters:
    Pmax = 0;   %Number of common points
    Cmax = 0;   %Cluster with higher overlap
    for c = 1:C
        mask2 = (L0==CList(c)); %Current cluster
        P = sum(mask2(:)&mask1(:));
        if P>Pmax
            Pmax = P;
            Cmax = CList(c);            
        end
    end
    U(mask1) = Cmax;
end
% nFinder = inline('min(x)<max(x)');
% mask = colfilt(U,[2 2], 'sliding', nFinder);
end