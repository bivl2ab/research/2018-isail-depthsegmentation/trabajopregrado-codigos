function PL = ucm2toNsp(ucm2,ki,Nmaxsp)

PL = ucm2toseg(ucm2, ki); 
U = unique(ucm2);    
[~,idx] = min(abs(ki - U));
Nsp = max(PL(:));
while Nsp > Nmaxsp
   PL = ucm2toseg(ucm2, U(idx));
   Nsp = max(PL(:));
   idx = idx + 1;
end   
 
end

%{
    tic
    PL = ucm2toseg(ucm2, 0.03); %0.03-?
    PL = cleanupregions(PL, 50);
    max(PL(:))
    toc

    for n = 1:290
        nTS = TrainSet(n);
        ucm2 = M_gPb_owt_ucm.ucm2(nTS,1); ucm2 = ucm2{1};
        PL = ucm2toseg(ucm2, 0.03); %0.03-?
        U(n) = max(PL(:));
    end    
       
    for n = 1:1449
        ucm2 = M_gPb_owt_ucm.ucm2(n,1); ucm2 = ucm2{1};
        PL = ucm2toseg(ucm2, 0.03); %0.03-?
        U(n) = max(PL(:));
    end   
    
    n = 167;
    ucm2 = M_gPb_owt_ucm.ucm2(n,1); ucm2 = ucm2{1};
    PL = ucm2toseg(ucm2, 0.03); %0.03-?
    figure,imshow(showseg(PL))
%}