function segShow = showseg2(rgb,seg)

seg = double(seg);

[X Y Z] = size(rgb);

img = im2double(rgb);

labels = seg(:);
L = max(labels); 


%% divide the image into multiple segments with different colors 
segColor = zeros(X,Y,3);
for i=1:L
    idx = find(labels==i);
    for j=1:3
        tmp = segColor(:,:,j); tmp1 = img(:,:,j);
        tmp(idx) = mean(tmp1(idx)); 
        segColor(:,:,j) = tmp; 
        clear tmp tmp1;
    end;
    clear idx;
end;

%%
B = boundarymask(seg, 4); 

segShow = imoverlay(segColor,B,'g');

%imwrite(segShow,imName);
   

end