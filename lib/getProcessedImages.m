function [n_image, n_rawDepth] = getProcessedImages(rgb_raw, dm_raw)

n_origDepth = dm_raw;
n_origRgb = rgb_raw;
[n_rawDepth, n_image] = project_depth_map(swapbytes(n_origDepth), n_origRgb);   
n_image = cropIt(n_image);

end


