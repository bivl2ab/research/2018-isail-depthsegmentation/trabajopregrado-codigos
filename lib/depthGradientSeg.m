function ucm2 = depthGradientSeg(x3, y3, z3)

disp('Computing 3D gradients....')

[ng1, ng2, dg, ~] = getGradients(x3, y3, z3);

dg_1 = dg(:,:,:,1);
dg_2 = dg(:,:,:,2);
dg_3 = dg(:,:,:,3);
dg_4 = dg(:,:,:,4);
clear dg
ng1_1 = ng1(:,:,:,1);
ng1_2 = ng1(:,:,:,2);
ng1_3 = ng1(:,:,:,3);
ng1_4 = ng1(:,:,:,4);
clear ng1
ng2_1 = ng2(:,:,:,1);
ng2_2 = ng2(:,:,:,2);
ng2_3 = ng2(:,:,:,3);
ng2_4 = ng2(:,:,:,4);
clear ng2

weights = [0.2 0.2 0.2 0.2 ...
           0.2 0.2 0.2 0.2 ...
           0.2 0.2 0.2 0.2];
gPb_orient = zeros(425,560,8);
for o = 1 : 8,
    a1 = weights(1)*dg_1(:, :, o);
    a2 = weights(2)*dg_2(:, :, o);
    a3 = weights(3)*dg_3(:, :, o);
    a4 = weights(4)*dg_4(:, :, o);

    b1 = weights(5)*ng1_1(:, :, o);
    b2 = weights(6)*ng1_2(:, :, o);
    b3 = weights(7)*ng1_3(:, :, o);
    b4 = weights(8)*ng1_4(:, :, o);

    c1 = weights(9)*ng2_1(:, :, o);
    c2 = weights(10)*ng2_2(:, :, o);
    c3 = weights(11)*ng2_3(:, :, o);
    c4 = weights(12)*ng2_4(:, :, o);

    gPb_orient(:, :, o) =  a1 + b1 + c1 + a2 + b2 + c2 + a3 + b3 + c3 + a4 + b4 + c4;
end

ucm2 = contours2ucm(gPb_orient, 'doubleSize');

end