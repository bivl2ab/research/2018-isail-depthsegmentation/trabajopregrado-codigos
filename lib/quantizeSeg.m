function uintSeg = quantizeSeg(seg)

Qseg = renumberregions(seg);

Nlabels = max(Qseg(:));

if Nlabels <= 255
    uintSeg = uint8(Qseg);
else
    uintSeg = uint16(Qseg);
end

end