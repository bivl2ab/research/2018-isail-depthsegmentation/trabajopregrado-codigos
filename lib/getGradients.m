function [ng1, ng2, dg, raw] = getGradients(x3, y3, z3)

depthParam = struct('qzc', 2.9e-5, ...
		'sigmaSpace', 1.40*[1 2 3 4], 'rr', 5*[1 2 3 4], ...
		'sigmaDisparity', [3 3 3 3], 'nori', 8, 'savgolFactor', 1.2);
pc(:,:,1) = x3; 
pc(:,:,2) = y3; 
pc(:,:,3) = z3;
pcf(:,:,1) = fillHoles(x3,'recursive-dilate');
pcf(:,:,2) = fillHoles(y3,'recursive-dilate');
pcf(:,:,3) = fillHoles(z3,'recursive-dilate');

[ng1, ng2, dg, raw] = computeDepthCues(pc, pcf, depthParam);

end