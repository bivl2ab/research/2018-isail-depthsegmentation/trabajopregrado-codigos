function segShow = showseg3(rgb,label_img,rad,name,col,display) %only_name,display)

label_img = double(label_img);

[X Y Z] = size(rgb);

img = im2double(rgb);

labels = label_img(:);
L = max(labels); 


%% divide the image into multiple segments with different colors 
segColor = zeros(X,Y,3);
for i=1:L
    idx = find(labels==i);
    for j=1:3
        tmp = segColor(:,:,j); tmp1 = img(:,:,j);
        tmp(idx) = mean(tmp1(idx)); 
        segColor(:,:,j) = tmp; 
        clear tmp tmp1;
    end;
    clear idx;
end;

%%
B = boundarymask(label_img, 4); 
if rad ~= 0
    B = imdilate(B, strel('disk',rad));
end
segShow = imoverlay(segColor,B,col);

%out_path = 'D:\Dropbox\Running3\pngs';
out_path = '/media/cvip/Datos/Isail/Dropbox/Running3/pngs';
imName = fullfile(out_path, [name,'_seg.png']);
imwrite(segShow,imName)
imName = fullfile(out_path, [name,'.png']);
imwrite(rgb,imName)


if display
    figure,imshow(segShow)
end    

end