function cbwseg = showseg(seg,display)

    colorLabels = label2rgb(seg, 'jet', [1 1 1], 'shuffle');
    cbwseg = imoverlay(colorLabels, boundarymask(seg,4),'w');

    if display
        figure,imshow(cbwseg)
    end    
end