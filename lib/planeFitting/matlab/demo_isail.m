addpath('mex')

load frame

subplot(1,2,1), imshow(frame.rgb)
subplot(1,2,2), imshow(frame.depth, [])
x3 = frame.xyz(:,:,1);
y3 = frame.xyz(:,:,2);
z3 = frame.xyz(:,:,3);
figure
pcshow(pointCloud(cat(3,x3,y3,z3), 'Color',  frame.rgb))

frame.mbs=fitAHCPlane(frame.xyz);
viewSeg(frame.mbs,640,480)

