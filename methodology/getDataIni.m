function [spData,spPairsData,Lab,h_edges] = getDataIni(rgb,L,Nsp)
    disp('Computing similarity measures for adjacent')
    disp(' pairs of superpixels....')

    % Pairs of adjacent superpixels in the PL
    [~, Al] = regionadjacency(L.P);
    spPairs = compute_spPairs(Al,Nsp);
    
    % Brightness, color, and texture cues of the sps in the PL
    Lab = rgb2lab(rgb); 
    % Histogram edges definition
    [~, h_edges.L] = histcounts(Lab(:,:,1));
    [~, h_edges.a]  = histcounts(Lab(:,:,2));
    [~, h_edges.b]  = histcounts(Lab(:,:,3));
    % Lab and textons channel histograms per superpixel
    spData = compute_spData(Lab,h_edges,L.P,1:Nsp);
    
    % Similarity between pairs of adjancent superpixels
    spPairsData = compute_spPairData(L,spData,spPairs); 
   
    
end    