function voteT = creaVoting(Ov,Ri,Rj,voters)

%uRh = unique(Ov(:)); 

uRh = count_unique(Ov);

%uRh = sort(Ov(:));
%uRh = uRh([true;diff(uRh(:))>0]);

LuRh = length(uRh);
vote_h = zeros(1,LuRh-1);
for h = 2:LuRh
    Rh = (Ov == uRh(h));
    vote_h(h-1) = (nnz(Ri & Rh)*nnz(Rj & Rh)) / voters; 
end

voteT = sum(vote_h);
    
end    