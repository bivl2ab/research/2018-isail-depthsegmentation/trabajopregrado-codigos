function spPairsData = compute_spPairData(L,spData,spPairs)

Npairs = size(spPairs,1);
spPairsData = struct('pair',cell(1,Npairs),...
					 'Sl',cell(1,Npairs),...
					 'Sa',cell(1,Npairs),...
					 'Sb',cell(1,Npairs),...
					 'voteSL1',cell(1,Npairs),...
					 'voteSL2',cell(1,Npairs),...
					 'voteSL3',cell(1,Npairs),...
					 'CREA',cell(1,Npairs),...
					 'Sjoint',cell(1,Npairs));
for p = 1:Npairs
    spPairsData(p).pair = spPairs(p,:);
    %% Appearance similarity
    % S^L (Ri, Rj)
    h1 = spData(spPairs(p,1)).hL; 
    h1 = h1/sum(h1);
    h2 = spData(spPairs(p,2)).hL;
    h2 = h2/sum(h2);
    x2 = ((h1-h2).^2)./(h1+h2);
    x2(isnan(x2)) = []; 
    x2 = 0.5*sum(x2);
    spPairsData(p).Sl = 1 - x2;
    % S^a (Ri, Rj)
    h1 = spData(spPairs(p,1)).ha; 
    h1 = h1/sum(h1);
    h2 = spData(spPairs(p,2)).ha;
    h2 = h2/sum(h2);
    x2 = ((h1-h2).^2)./(h1+h2);
    x2(isnan(x2)) = []; 
    x2 = 0.5*sum(x2);
    spPairsData(p).Sa = 1 - x2;
    % S^b (Ri, Rj)
    h1 = spData(spPairs(p,1)).hb; 
    h1 = h1/sum(h1);
    h2 = spData(spPairs(p,2)).hb;
    h2 = h2/sum(h2);
    x2 = ((h1-h2).^2)./(h1+h2);
    x2(isnan(x2)) = []; 
    x2 = 0.5*sum(x2);
    spPairsData(p).Sb = 1 - x2;
    %% Cross-region evidence accumulation 
    Ri = (L.P == spPairs(p,1));  
    Rj = (L.P == spPairs(p,2)); 
    voters = nnz(Ri)*nnz(Rj);
    % vote^k (Ri, Rj)  w.r.t. SL1
    Ov = (L.S1).*(Ri | Rj);	
	voteT = creaVoting(Ov,Ri,Rj,voters);
    spPairsData(p).voteSL1 = voteT;
    % vote^k (Ri, Rj)  w.r.t. SL2
    Ov = (L.S2).*(Ri | Rj);
  	voteT = creaVoting(Ov,Ri,Rj,voters);
    spPairsData(p).voteSL2 = voteT;
    % vote^k (Ri, Rj)  w.r.t. SL3
    Ov = (L.S3).*(Ri | Rj);
    voteT = creaVoting(Ov,Ri,Rj,voters);
    spPairsData(p).voteSL3 = voteT;
end    

end
 