function spData = compute_spData(Lab,h_edges,PL,sps) 

L = Lab(:,:,1); a = Lab(:,:,2); b = Lab(:,:,3); 
Nsps = length(sps);
spData = struct('hL',cell(1,Nsps),'ha',cell(1,Nsps),'hb',cell(1,Nsps));
for k = 1:Nsps
    ksp_L = L(PL == sps(k));
    ksp_a = a(PL == sps(k));
    ksp_b = b(PL == sps(k));
    spData(k).hL = histcounts(ksp_L, h_edges.L);
    spData(k).ha = histcounts(ksp_a, h_edges.a);  
    spData(k).hb = histcounts(ksp_b, h_edges.b);
end

end