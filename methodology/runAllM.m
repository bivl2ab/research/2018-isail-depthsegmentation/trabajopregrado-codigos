function Tree = runAllM(n,rgb,dm,C,bol1,bol2,M_dg_owt_ucm,L,Param)

    % L.S2: Depth Contour Segmentation
    % L.S3: Planar Segmentation
    [L.S2, L.S3] = getDepthLayers(n,rgb,dm,C,bol1,M_dg_owt_ucm);
    %unique(L.S2); 
    %unique(L.S3);
    
    %showseg(L.P,1); showseg(L.S1,1); showseg(L.S2,1); showseg(L.S3,1);  
   
    
    Nsp = max(L.P(:));
    % Similarity Calculation
    if bol2
       [spData,spPairsData,Lab,h_edges] = getDataIni(rgb,L,Nsp,bol2); 
    else
       [spData,~,Lab,h_edges] = getDataIni(rgb,L,Nsp,~bol2); 
        spPairsData = DATA(n).spPairsData;
    end
    
    % Joint Similarity between pairs of adjacent superpixels
    spPairsData = complete_spPairData(spPairsData,Param);
    
    % Hirarchical region merging
    Tree = computeTree(spData,spPairsData,L,Nsp,Lab,h_edges,Param);
    % Stop when Sjoint < 0.5-0.6
	
end	