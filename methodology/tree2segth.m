function seg = tree2segth(PL, Tree, SjointThr)

Nsp = max(PL(:));
i = 1;
Maxlevel = length(Tree);
while Tree(i).SjointMax >= SjointThr
    PL(PL == Tree(i).spPairMerge(1)) = Nsp + i; 
    PL(PL == Tree(i).spPairMerge(2)) = Nsp + i;
    i = i+1;
    if i > Maxlevel
        disp('Flevel Ok')
        break
    end    
end
seg = PL;

%{
Nsp = max(PL(:));
level = length(Tree);
for i = 1:level
    PL(PL == Tree(i).spPairMerge(1)) = Nsp + i; 
    PL(PL == Tree(i).spPairMerge(2)) = Nsp + i;
end
seg = PL;
%}
end    
    
