function Tree = computeTree(spData,spPairsData,L,Nsp,Lab,h_edges,Param)

disp('Computing Hierarchical Segmentation Tree...')

Levels = Nsp - 2;
Npairs = length(spPairsData);

% Hirarchical region merging
for i = 1:Levels

    disp(sprintf('Tree Level: %d',i))

    [SjointMax, ind] = max([spPairsData(:).Sjoint]);
    spPairMerge = spPairsData(ind).pair;
    Tree(i).spPairMerge = spPairMerge;
    Tree(i).SjointMax = SjointMax;
    if Tree(i).SjointMax <= Param.SjTHR
        Tree(i) = [];
        %disp(sprintf('Selected Level: %d',i-1))
        break
    end 

    L.P(L.P == spPairMerge(1)) = Nsp + i; 
    L.P(L.P == spPairMerge(2)) = Nsp + i;

    % Lab and textons channel histograms (new superpixel)
    spData(Nsp+i) = compute_spData(Lab,h_edges,L.P,Nsp+i);

    % Adjacent pairs (new superpixel)
    [~, Al_new] = regionadjacency(L.P); Al_new = Al_new{Nsp+i};
    Npairs_new = length(Al_new);
    spPairs_new = [(Nsp+i)*ones(Npairs_new,1), Al_new'];

    % Similarity between adjancent pairs (new superpixel)
    spPairsData(Npairs+1:Npairs+Npairs_new) = compute_spPairData(L,spData,spPairs_new);

    % Joint Similarity between adjacent pairs (new superpixel)
    spPairsData(Npairs+1:Npairs+Npairs_new) = ...
        complete_spPairData(spPairsData(Npairs+1:Npairs+Npairs_new),Param);

    % Remove old pairs (merged superpixels)
    Npairs = Npairs+Npairs_new;
    for p = 1:Npairs
        bol1 = ( spPairsData(p).pair(1) == spPairMerge(1) | spPairsData(p).pair(1) == spPairMerge(2) );
        bol2 = ( spPairsData(p).pair(2) == spPairMerge(1) | spPairsData(p).pair(2) == spPairMerge(2) );
        if (bol1 | bol2)      
            spPairsData(p).pair = [NaN,NaN];
            spPairsData(p).Sl = [];
            spPairsData(p).Sa = [];
            spPairsData(p).Sb = [];
            spPairsData(p).voteSL1 = [];
            spPairsData(p).voteSL2 = [];
            spPairsData(p).voteSL3 = [];
            spPairsData(p).CREA = [];
            spPairsData(p).Sjoint = NaN;
        end
    end

end
    
end    
