function spPairsData = complete_spPairData(spPairsData,Param)

Npairs = length(spPairsData);

for p = 1:Npairs
    % CREA calculation
    spPairsData(p).CREA = ( spPairsData(p).voteSL1 + ...
                            spPairsData(p).voteSL2 + ...
                            spPairsData(p).voteSL3 )/3;    
    % Joint similarity               
    spPairsData(p).Sjoint = (1-Param.Las)*spPairsData(p).CREA + ...
            Param.Las*( spPairsData(p).Sl + spPairsData(p).Sa + spPairsData(p).Sb )/3; 
end
       
end