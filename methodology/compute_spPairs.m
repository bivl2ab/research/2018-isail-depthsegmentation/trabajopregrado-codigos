function spPairs = compute_spPairs(Al,Nsp) 

spPairs_total = [];
for k = 1:Nsp
    Nadj = length(Al{k});
    spPairs_act = [k*ones(1,Nadj); Al{k}];
    spPairs_total = [spPairs_total spPairs_act];
end
spPairs = unique(sort(spPairs_total',2), 'rows');

end