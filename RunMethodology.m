%%%% A Methodology fo Image Segmentation Using %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% Superpixels and Depth Information %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Setting %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath(genpath('lib'))
addpath(genpath('benchmarks'))
addpath(genpath('methodology'))
addpath(genpath('rgbSegs'))

%% Parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C = getCameraParam('color');

Param.Las = 0.4;
Param.SjTHR = 0.59;

%% Load data %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('rgb_raw.mat');
load('dm_raw.mat');
load('gt.mat');

figure(1)
subplot(1,2,1), imshow(rgb_raw), title('Raw RGB')
subplot(1,2,2), imshow(dm_raw,[]), title('Raw Depth Map')
pause()

%% Pipeline %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% RGB-D Image Pre-Processing
[rgb, dm] = getProcessedImages(rgb_raw, dm_raw);

figure(2)
subplot(1,2,1), imshow(rgb), title('Pre-Processed RGB')
subplot(1,2,2), imshow(dm,[]), title('Pre-Processed Depth Map')
pause()
%% Segmentation Layers Generation
% L.P: RGB Superpixels
disp('Obtaining superpixels...')
load('ucm2.mat')
L.P = ucm2toNsp(ucm2, 0.03, 400);
L.P = cleanupregions(L.P, 25);

% L.S1: RGB Segmentation
disp('Obtaining classical segmentation...')
L.S1 = ucm2toNsp(ucm2, 0.12, 200); 
L.S1 = cleanupregions(L.S1, 50);

figure(3)
subplot(1,2,1), imshow(showseg2(rgb,L.P)), title('Superpixel Segmentation')
subplot(1,2,2), imshow(showseg2(rgb,L.S1)), title('Classical Segmentation')
pause()

% 3D Point Cloud
[x3 y3 z3] = getPointCloudFromZ(dm, C);

try
pc = pointCloud(cat(3,x3,y3,z3),'Color',rgb);
figure(4)
pcshow(pc,'VerticalAxis','y','VerticalAxisDir','down')
xlabel('x (cm)'), ylabel('y (cm)'), zlabel('z (cm)')
title('3D Point Cloud')
pause()
catch
disp('The 3D point cloud could not be plotted :(')
end
    
% L.S2: 3D-Edge Segmentation
ucm2 = depthGradientSeg(x3, y3, z3);
L.S2 = ucm2toNsp(ucm2, 0.35, 200); 
L.S2 = cleanupregions(L.S2, 50);

% L.S3: Planar Segmentation
L.S3 = planarSeg(x3, y3, z3);

figure(5)
subplot(1,2,1), imshow(showseg2(rgb,L.S2)), title('3D-Edge Segmentation')
subplot(1,2,2), imshow(showseg(L.S3,0)), title('Planar Segmentation')
pause()
% Filling empty pixels in planar segmentation with 3d-edge segmentation
L.S3(L.S3 ~= 0) = L.S3(L.S3 ~= 0) + max(L.S2(:));
L.S3(L.S3 == 0) = L.S2(L.S3 == 0);
L.S3 = cleanupregions(L.S3, 50);

figure(6)
subplot(1,2,1), imshow(showseg2(rgb,L.S2)), title('3D-Edge Segmentation')
subplot(1,2,2), imshow(showseg2(rgb,L.S3)), title('Planar Segmentation (Filled)')
pause()
%% Hierarchical Region Merging
Nsp = max(L.P(:));
% CREA and Appearance between all pairs of adjacent superpixels
[spData,spPairsData,Lab,h_edges] = getDataIni(rgb,L,Nsp); 
% Joint Similarity between all pairs of adjacent superpixels
spPairsData = complete_spPairData(spPairsData,Param);
% Region merging process
Tree = computeTree(spData,spPairsData,L,Nsp,Lab,h_edges,Param);

segProp = tree2segth(L.P,Tree,0.59); 

figure(7)
subplot(1,2,1), imshow(showseg2(rgb,L.S1)), title('Classical Segmentation')
subplot(1,2,2), imshow(showseg2(rgb,segProp)), title('Proposed Segmentation')
pause()

%% Performance Evaluation
Mprop = benchmarking(segProp,gt); 
Mclassical = benchmarking(L.S1,gt);
disp('Segmentation Covering - Classical Segmentation')
disp(Mclassical(1))
disp('Segmentation Covering - Proposed Segmentation')
disp(Mprop(1))




