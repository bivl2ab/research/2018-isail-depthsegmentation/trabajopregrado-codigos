function [bestR, bgRI, bgVOI] = ... 
            collect_eval_reg(cntR, sumR, cntP, sumP, cntR_best, sumRI, sumVOI)
    
cntR_total = 0;
sumR_total = 0;
cntP_total = 0;
sumP_total = 0;
cntR_best = 0;
sumR_best = 0;
cntP_best = 0;
sumP_best = 0;
globalRI = 0;
globalVOI = 0;
RI_best = 0;
VOI_best = 0;

cntR_best_total = 0;

scores = zeros(1,4);

i = 1;

thresh = 1;

tmp = [1, cntR, sumR, cntP, sumP];

R = cntR ./ (sumR + (sumR==0));
P = cntP ./ (sumP + (sumP==0));

[bestR ind] = max(R);
bestT = thresh(ind(1));
bestP = P(ind(1));
scores(i,:) = [i bestT bestR bestP ];

cntR_total = cntR_total + cntR;
sumR_total = sumR_total + sumR;
cntP_total = cntP_total + cntP;
sumP_total = sumP_total + sumP;

ff=find(R==max(R));
cntR_best = cntR_best + cntR(ff(end));
sumR_best = sumR_best + sumR(ff(end));
cntP_best = cntP_best + cntP(ff(end));
sumP_best = sumP_best + sumP(ff(end));

cntR_best_total = cntR_best;

globalRI = globalRI + sumRI;
globalVOI = globalVOI + sumVOI;

ff = find( tmp(:,2)==max(tmp(:,2)) );
RI_best = RI_best + tmp(ff(end),2);
ff = find( tmp(:,3)==min(tmp(:,3)) );
VOI_best = VOI_best + tmp(ff(end),3);

R = cntR_total ./ (sumR_total + (sumR_total==0));

[bestR ind] = max(R);
bestT = thresh(ind(1));


R_best = cntR_best ./ (sumR_best + (sumR_best==0));

R_best_total = cntR_best_total / sumR_total(1) ;



globalRI = globalRI / 1;
globalVOI = globalVOI / 1;
RI_best = RI_best / 1;
VOI_best = VOI_best / 1;
[bgRI igRI]=max(globalRI);
[bgVOI igVOI]=min(globalVOI);






