SET

%% Examples of benchmarks for different input formats
addpath benchmarks
clear all;close all;clc;

tic
eval_segmentation(double(segs{1}), {double(groundTruth{1}.Segmentation)})
toc

outDir = 'bench_temps';
evFile2 = 'bench_temps/temp_ev2.txt';
evFile3 = 'bench_temps/temp_ev3.txt';
evFile4 = 'bench_temps/temp_ev4.txt';
tic
evaluation_reg_image(segs, groundTruth, evFile2, evFile3, evFile4, 1);
collect_eval_reg(outDir);
[C, PRI, VoI] = plot_eval(outDir)
delete(sprintf('%s/*.txt', outDir));
toc

