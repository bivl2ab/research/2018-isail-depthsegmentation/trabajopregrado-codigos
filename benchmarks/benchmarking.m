function Measures = benchmarking(seg, gt)


groundTruth{1}.Segmentation = gt;
[cntR,sumR,cntP,sumP,cntR_best,sumRI,sumVOI] = ...
                    evaluation_reg_image(seg, groundTruth);

[Cov, PRI, VoI] = ...
    collect_eval_reg(cntR, sumR, cntP, sumP, cntR_best, sumRI, sumVOI);

BDE = compare_image_boundary_error(seg, gt);

Measures = [Cov, PRI, VoI, BDE];

end



