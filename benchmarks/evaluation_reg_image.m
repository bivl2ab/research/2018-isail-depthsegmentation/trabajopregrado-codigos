function [cntR,sumR,cntP,sumP,cntR_best,sumRI,sumVOI] = ...
                        evaluation_reg_image(seg, groundTruth)


regionsGT = [];
total_gt = 0;

s = 1;
groundTruth{s}.Segmentation = double(groundTruth{s}.Segmentation);
regionsTmp = regionprops(groundTruth{s}.Segmentation, 'Area');
regionsGT = [regionsGT; regionsTmp];
total_gt = total_gt + max(groundTruth{s}.Segmentation(:));


% zero all counts
cntR = 0;
sumR = 0;
cntP = 0;
sumP = 0;
sumRI = 0;
sumVOI = 0;

best_matchesGT = zeros(1, total_gt);

seg = double(seg);


[ri voi] = match_segmentations2(seg, groundTruth);
sumRI = ri;
sumVOI = voi;

[matches] = match_segmentations(seg, groundTruth);
matchesSeg = max(matches, [], 2);
matchesGT = max(matches, [], 1);

regionsSeg = regionprops(seg, 'Area');
for r = 1 : numel(regionsSeg)
    cntP = cntP + regionsSeg(r).Area*matchesSeg(r);
    sumP = sumP + regionsSeg(r).Area;
end

for r = 1 : numel(regionsGT),
    cntR = cntR +  regionsGT(r).Area*matchesGT(r);
    sumR = sumR + regionsGT(r).Area;
end

best_matchesGT = max(best_matchesGT, matchesGT);


% output
cntR_best = 0;
for r = 1 : numel(regionsGT),
    cntR_best = cntR_best +  regionsGT(r).Area*best_matchesGT(r);
end



