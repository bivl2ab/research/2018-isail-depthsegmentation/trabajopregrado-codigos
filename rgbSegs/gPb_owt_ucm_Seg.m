function segQ = gPb_owt_ucm_Seg(ucm2,ki,Nsegments) 
        %ucm2 = gPb_owt_ucm_Seg(rgb) 

%% im2seg
%{
folder = './rgbSegs/gPb-owt-ucm/data';
imwrite(rgb, fullfile(folder, 'im_bsd.bmp'))
imgFile = 'im_bsd.bmp';
outFile = 'rgbSegs/gPb-owt-ucm/data/im_bsd.mat';
gPb_orient = globalPb_pieces(imgFile, outFile);
ucm2 = contours2ucm(gPb_orient, 'doubleSize');
%}
%%
if nargin == 2
    seg = ucm2toseg(ucm2,ki);
    segQ = quantizeSeg(seg);
else
    seg = ucm2toNsp(ucm2,ki,Nsegments);
    segQ = quantizeSeg(seg);
end    


end
