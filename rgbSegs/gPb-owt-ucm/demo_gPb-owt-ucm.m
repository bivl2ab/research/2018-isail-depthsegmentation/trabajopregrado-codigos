%% Compute globalPb and hierarchical segmentation for an example image.
tic
%% 1. compute globalPb on a BSDS image (5Gb of RAM required)
dir = './support/Arbelaez';
addpath(genpath(dir))
imgFile = '201.png';
outFile = 'support/Arbelaez/data/temp.mat'
gPb_orient = globalPb_pieces(imgFile, outFile);
%% 2. compute Hierarchical Regions
% for regions 
ucm2 = contours2ucm(gPb_orient, 'doubleSize');
ucm = ucm2(3:2:end, 3:2:end);
%% 3. usage example
% get the boundaries of segmentation at scale k in range [0 1]
k = 0.25;
% get superpixels at scale k without boundaries:
labels2 = bwlabel(ucm2 <= k);
labels = labels2(2:2:end, 2:2:end);
figure,imshow(label2rgb(labels, 'jet', [1 1 1], 'shuffle'))
toc


%% im2seg
cd lib/rgbseg/
folder = './support/Arbelaez/data';
imwrite(im, fullfile(folder, 'im_bsd.bmp'))
imgFile = 'im_bsd.bmp';
outFile = 'support/Arbelaez/data/im_bsd.mat';
gPb_orient = globalPb_pieces(imgFile, outFile);
ucm2 = contours2ucm(gPb_orient, 'doubleSize');
seg = ucm2;
cd ..
cd ..