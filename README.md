Code for the Grade Project:
"A Methodology for Image Segmentation Using Superpixels and Depth Information"
Author: Isail Salazar - 2122307, Electronic Engineering
Advisor: Fabio Martínez, PhD
Co-Advisor: Said Pertuz, PhD

Contact E-mail: isail1995@hotmail.com

This code is destined to run under Linux OS
Tested on Ubuntu 14.04 with MATLAB R2016B
Somo recent function were used, therefore, it is recommended to run in
recent versions: R2016A, R2016B, R2017A, R2017B. 

To compute and visualize the segmentation results for a sample RGB-D image,
run the script "RunMethodology.m"

Description of provided mat-files:
rgb_raw.mat => Raw RGB image, as provided by Kinect
dm_raw.mat = > Raw Depth Map, as provided by Kinect
gt.mat => Ground-truth Segmentation
These files were generated from the NYUD2 Dataset:
http://cs.nyu.edu/~silberman/datasets/nyu_depth_v2.html

The segmentation based on RGB data was pre-calculated and is provided by the 
file "ucm2.mat", this is a hierarchical segmentation tree, as obtained by 
running the gPb-OWT-UCM algorithm:
https://www2.eecs.berkeley.edu/Research/Projects/CS/vision/grouping/resources.html

From this tree, the superpixel and the classical segmentation are obtained. 

 
